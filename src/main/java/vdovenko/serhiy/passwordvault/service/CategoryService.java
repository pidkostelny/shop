package vdovenko.serhiy.passwordvault.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vdovenko.serhiy.passwordvault.dto.request.CategoryRequest;
import vdovenko.serhiy.passwordvault.dto.response.CategoryResponse;
import vdovenko.serhiy.passwordvault.entity.Category;
import vdovenko.serhiy.passwordvault.repository.CategoryRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public void create(CategoryRequest request) {
        categoryRepository.save(categoryRequestToCategory(null, request));
    }

    public List<CategoryResponse> findAll() {
        return categoryRepository.findAll().stream().map(CategoryResponse::new).collect(Collectors.toList());
    }

    public void update(CategoryRequest request, Long id) {
        categoryRepository.save(categoryRequestToCategory(findOne(id), request));
    }

    public void delete(Long id) {
        categoryRepository.delete(findOne(id));
    }

    public Category findOne(Long id) {
        return categoryRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Category with id " + id + " not exists"));
    }

    public CategoryResponse findOneResponse(Long id) {
        return new CategoryResponse(findOne(id));
    }

    private Category categoryRequestToCategory(Category category, CategoryRequest request) {
        if (category == null) {
            category = new Category();
        }
        category.setName(request.getName());
        return category;
    }
}
