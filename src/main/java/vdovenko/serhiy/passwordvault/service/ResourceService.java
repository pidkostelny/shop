package vdovenko.serhiy.passwordvault.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vdovenko.serhiy.passwordvault.dto.request.ResourceRequest;
import vdovenko.serhiy.passwordvault.dto.response.ResourceResponse;
import vdovenko.serhiy.passwordvault.entity.Category;
import vdovenko.serhiy.passwordvault.entity.Resource;
import vdovenko.serhiy.passwordvault.repository.ResourceRepository;
import vdovenko.serhiy.passwordvault.tool.FileTool;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ResourceService {


    public static final String ICON_DIR =
            System.getProperty("user.home") + File.separator +
                    "resources-icons" + File.separator;

//    "/home/kindgeek/resources-icons/"
//    "C:\Users\Study\resources-icons\"

    @Autowired
    private ResourceRepository resourceRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private FileTool fileTool;

    public void create(ResourceRequest request) throws IOException {
        resourceRepository.save(resourceRequestToResource(new Resource(), request));
    }

    public List<ResourceResponse> findAll() {
        return resourceRepository.findAll().stream().map(ResourceResponse::new).collect(Collectors.toList());
    }

    public void update(ResourceRequest request, Long id) throws IOException {
        resourceRepository.save(resourceRequestToResource(findOne(id), request));
    }

    public void delete(Long id) {
        resourceRepository.delete(findOne(id));
    }

    public Resource findOne(Long id) {
        return resourceRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Resource with id " + id + " not existss"));
    }

    private Resource resourceRequestToResource(Resource resource, ResourceRequest request) throws IOException {
        if (request.getIcon() != null && !request.getIcon().isEmpty()) {
            String pathToFile = fileTool.saveFile(request.getIcon(), ICON_DIR);
            resource.setIcon(pathToFile);
        }
        final Category category = categoryService.findOne(request.getCategoryId());
        resource.setCategory(category);
        resource.setName(request.getName());
        resource.setDescription(request.getDescription());
        return resource;
    }
}
