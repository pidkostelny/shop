package vdovenko.serhiy.passwordvault.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vdovenko.serhiy.passwordvault.dto.request.CategoryRequest;
import vdovenko.serhiy.passwordvault.dto.response.CategoryResponse;
import vdovenko.serhiy.passwordvault.service.CategoryService;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @PostMapping
    public void create(@RequestBody CategoryRequest request) {
        categoryService.create(request);
    }

    @GetMapping
    public List<CategoryResponse> findAll() {
        return categoryService.findAll();
    }

    @GetMapping("/byId")
    public CategoryResponse findOne(Long id) {
        return categoryService.findOneResponse(id);
    }

    @PutMapping
    public void update(@RequestBody CategoryRequest request, Long id) {
        categoryService.update(request, id);
    }

    @DeleteMapping
    public void delete(Long id) {
        categoryService.delete(id);
    }
}
