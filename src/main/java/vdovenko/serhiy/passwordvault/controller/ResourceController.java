package vdovenko.serhiy.passwordvault.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vdovenko.serhiy.passwordvault.dto.request.ResourceRequest;
import vdovenko.serhiy.passwordvault.dto.response.ResourceResponse;
import vdovenko.serhiy.passwordvault.service.ResourceService;

import java.io.IOException;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/resource")
public class ResourceController {

    @Autowired
    private ResourceService resourceService;

    @PostMapping
    public void create(@RequestBody ResourceRequest request) throws IOException {
        resourceService.create(request);
    }

    @GetMapping
    public List<ResourceResponse> findAll() {
        return resourceService.findAll();
    }

    @PutMapping
    public void update(@RequestBody ResourceRequest request, @RequestParam Long id) throws IOException {
        resourceService.update(request, id);
    }

    @DeleteMapping
    public void delete(Long id) {
        resourceService.delete(id);
    }
}
