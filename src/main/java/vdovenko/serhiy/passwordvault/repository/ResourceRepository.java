package vdovenko.serhiy.passwordvault.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import vdovenko.serhiy.passwordvault.entity.Resource;

import java.util.List;

@Repository
public interface ResourceRepository extends JpaRepository<Resource, Long> {

    @Query("from Resource r left join r.category c where c.name like :name")
    List<Resource> findByCategoryName(@Param("name") String categoryName);
}
