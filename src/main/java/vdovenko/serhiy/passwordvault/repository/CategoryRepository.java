package vdovenko.serhiy.passwordvault.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import vdovenko.serhiy.passwordvault.entity.Category;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

//    @Query("from Category c where c.name like :aaa")
//    List<Category> findByNameLike(@Param("aaa") String name);

    List<Category> findAllByNameLike(String name);
}
