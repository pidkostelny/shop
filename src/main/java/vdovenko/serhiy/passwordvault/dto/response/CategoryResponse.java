package vdovenko.serhiy.passwordvault.dto.response;

import lombok.Getter;
import lombok.Setter;
import vdovenko.serhiy.passwordvault.entity.Category;

@Setter
@Getter
public class CategoryResponse {
    private String name;
    private Long id;

    public CategoryResponse(Category category) {
        name = category.getName();
        id = category.getId();
    }
}
