package vdovenko.serhiy.passwordvault.dto.response;

import lombok.Getter;
import lombok.Setter;
import vdovenko.serhiy.passwordvault.entity.Resource;

@Getter
@Setter
public class ResourceResponse {
    private Long id;
    private String name;
    private String description;
    private String icon;
    private CategoryResponse categoryResponse;

    public ResourceResponse(Resource resource) {
        name = resource.getName();
        id = resource.getId();
        description = resource.getDescription();
        icon = resource.getIcon();
        categoryResponse = new CategoryResponse(resource.getCategory());
    }
}
