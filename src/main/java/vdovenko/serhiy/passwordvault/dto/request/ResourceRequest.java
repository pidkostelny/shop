package vdovenko.serhiy.passwordvault.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResourceRequest {
    private String name;
    private String description;
    private String icon;
    private Long categoryId;
}
