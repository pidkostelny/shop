package vdovenko.serhiy.passwordvault.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class Resource {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String icon;
    private String name;
    private String description;

    @ManyToOne
    private Category category;

    @OneToMany(mappedBy = "resource")
    private List<Credential> credentials = new ArrayList<>();


}
