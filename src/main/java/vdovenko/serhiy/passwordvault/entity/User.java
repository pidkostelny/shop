package vdovenko.serhiy.passwordvault.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String lastName;
    private String firstName;
    private String username;
    private String password;
    private String passwordHint;

    @OneToMany(mappedBy = "user")
    private List<Credential> credentials = new ArrayList<>();


}
