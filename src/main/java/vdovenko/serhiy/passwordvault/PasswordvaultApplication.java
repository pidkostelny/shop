package vdovenko.serhiy.passwordvault;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import vdovenko.serhiy.passwordvault.entity.Category;
import vdovenko.serhiy.passwordvault.entity.Resource;
import vdovenko.serhiy.passwordvault.repository.CategoryRepository;
import vdovenko.serhiy.passwordvault.repository.ResourceRepository;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Controller
@SpringBootApplication
public class PasswordvaultApplication {

    public static void main(String[] args) {
        SpringApplication.run(PasswordvaultApplication.class, args);
    }


}
