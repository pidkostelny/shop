package vdovenko.serhiy.passwordvault.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.file.Paths;

import static vdovenko.serhiy.passwordvault.service.ResourceService.ICON_DIR;

@Configuration
public class StaticResourcesConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/icon/**")
                .addResourceLocations(Paths.get(ICON_DIR).toUri().toString());
    }
}
